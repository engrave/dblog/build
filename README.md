# Engrave

# Running DEVELOPMENT for the first time:

1. Clone `build` repository
2. Run `./clone-all-repositories.sh`
3. Run `./build-local-images.sh`
4. Run `./request-wildcard-certificate.sh` and follow instructions for `musk.rocks` domain
5. Create docker secret with `echo "your-steemconnect-app-secret" | sudo docker secret create SC2_APP_SECRET -`
6. Run `./run-development-stack.sh`
7. Visit `https://musk.rocks` in your browser


# Running production or staging for the first time:

1. Configure MongoDB replica set

Connect to `mongo-primary` database and run following command:

```
rs.initiate({
    _id: "replica-set", 
    members: [
        { _id: 0, host: "mongo-primary:27017", priority : 3},
        { _id: 1, host: "mongo-worker-one:27017", priority : 1 },
        { _id: 2, host: "mongo-worker-two:27017", priority : 1 }
    ]
})
```

**Important**: do it only for the first run (empty database). Configuration in stored in persistance volume and it's not necessary to run it everytime container is launched.


2. Configure Vault and set Vault Token as docker secret or CI environment variable.

Attach to vault container, add ADDR as http and initialize secrets:

```
vault login -address="http://127.0.0.1:8200"
vault secrets enable -address="http://127.0.0.1:8200" -path=secret/ kv
```
