#!/bin/bash

set -e

docker build -t engrave-api-gateway:dev ../backend/api-gateway --target development
docker build -t engrave-auth:dev ../backend/auth --target development
docker build -t engrave-blockchain-tracker:dev ../backend/blockchain-tracker --target development
docker build -t engrave-domains-manager:dev ../backend/domains-manager --target development
docker build -t engrave-image-uploader:dev ../backend/image-uploader --target development
docker build -t engrave-mailer:dev ../backend/mailer --target development
docker build -t engrave-nginx-configurator:dev ../backend/nginx-configurator --target development
docker build -t engrave-sitemap-builder:dev ../backend/sitemap-builder --target development
docker build -t engrave-ssl-manager:dev ../backend/ssl-manager --target development
docker build -t engrave-statistics-manager:dev ../backend/statistics-manager --target development
docker build -t engrave-vault-connector:dev ../backend/vault-connector --target development
docker build -t engrave-webpush-sender:dev ../backend/webpush-sender --target development

docker build -t engrave-blogs-renderer:dev ../frontend/blogs-renderer --target development
docker build -t engrave-dashboard:dev ../frontend/dashboard --target development
docker build -t engrave-main-website:dev ../frontend/main-website --target development

docker build -t engrave-nginx:dev ../nginx
