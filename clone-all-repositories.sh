#!/bin/bash
set -e

# Define the base GitLab group project path
BASE_PATH="git@gitlab.com:engrave/dblog"

# Function to clone a repository and initialize submodules
clone_repository() {
    local repo_name=$1

    git clone --recursive "$BASE_PATH/$repo_name.git" "../$repo_name"
}

echo " * Preparing necessary directories"
mkdir -p ../backend
mkdir -p ../frontend

echo " * Cloning all backend repositories"

backend=(
    "api-gateway"
    "blockchain-tracker"
    "domains-manager"
    "image-uploader"
    "mailer"
    "nginx-configurator"
    "sitemap-builder"
    "ssl-manager"
    "vault-connector"
    "webpush-sender"
)

for backend_repo in "${backend[@]}"; do
    clone_repository "backend/$backend_repo"
done

echo " * Cloning all frontend repositories"

frontend=(
    "blogs-renderer"
    "main-website"
)

for frontend_repo in "${frontend[@]}"; do
    clone_repository "frontend/$frontend_repo"
done

echo " * Cloning all top-level repositories"

toplevel=(
    "nginx"
    "shared-library"
    "gitlab-shared"
)

for toplevel_repo in "${toplevel[@]}"; do
    clone_repository "$toplevel_repo"
done

exit 0
