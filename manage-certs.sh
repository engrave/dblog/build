#!/bin/bash

set -e # exit immediately on first error

read -rp 'Enter Bitwarden machine access token: ' token

name_com_username=$(bws secret get --access-token "$token" "587b167e-7f91-41b5-b551-b28301778661" | jq -r '.value')
name_com_api_key=$(bws secret get --access-token "$token" "c416029f-4bba-4c3d-8bb6-b2830177a18e" | jq -r '.value')
ovh_application_key=$(bws secret get --access-token "$token" "c22b1d3d-a615-4778-94cc-b2830177d87a" | jq -r '.value')
ovh_application_secret=$(bws secret get --access-token "$token" "1bad3ebc-6e6d-4c30-afbf-b2830177eea3" | jq -r '.value')
gitlab_token=$(bws secret get --access-token "$token" "b6cb7288-f940-41b8-98f4-b28301780fe3" | jq -r '.value')
acme_account_email=$(bws secret get --access-token "$token" "0450a81b-4962-4e85-a6b4-b283017fdc70" | jq -r '.value')

echo I will try to generate SSL certificates for dblog.org, engrave.site and engrave.website

mkdir -p "$(realpath .)/certs"

# Register ACME account if necessary
docker run -it --rm --name acme \
    -e Namecom_Username="$name_com_username" \
    -e Namecom_Token="$name_com_api_key" \
    -v "$(realpath .)/certs:/acme.sh" \
    neilpang/acme.sh \
        acme.sh \
        --register-account -m "$acme_account_email"

# NOTE
# In case of any problems with this ca, try removing --set-default-ca --server letsencrypt and it will use zerossl

docker run -it --rm --name acme \
    -e Namecom_Username="$name_com_username" \
    -e Namecom_Token="$name_com_api_key" \
    -v $(realpath .)/certs:/acme.sh \
    neilpang/acme.sh \
        acme.sh \
        --set-default-ca --server letsencrypt \
        --issue \
        -d dblog.org \
        -d *.dblog.org \
        -d engrave.site --challenge-alias dblog.org \
        -d *.engrave.site --challenge-alias dblog.org \
        -d engrave.website --challenge-alias dblog.org \
        -d *.engrave.website --challenge-alias dblog.org \
        --dns dns_namecom \
        --keylength 2048

docker run -it --rm --name acme \
    -v "$(realpath .)/certs:/acme.sh" \
    neilpang/acme.sh \
        --install-cert \
        -d dblog.org \
        -d *.dblog.org \
        -d engrave.site \
        -d *.engrave.site \
        -d engrave.website \
        -d *.engrave.website \
        --cert-file      /acme.sh/dblog.org/cert.pem  \
        --key-file       /acme.sh/dblog.org/key.pem  \
        --fullchain-file /acme.sh/dblog.org/fullchain.pem


echo I will try to generate SSL certificates for dblog.pl


mkdir -p "$(realpath .)/certs"

docker run -it --rm --name acme \
    -e OVH_AK="$ovh_application_key" \
    -e OVH_AS="$ovh_application_secret" \
    -v "$(realpath .)/certs:/acme.sh" \
    neilpang/acme.sh \
        acme.sh \
        --issue \
        -d dblog.pl \
        -d *.dblog.pl \
        --dns dns_ovh \
        --keylength 2048

docker run -it --rm --name acme \
    -v "$(realpath .)/certs:/acme.sh" \
    neilpang/acme.sh \
        --install-cert \
        -d dblog.pl \
        -d *.dblog.pl \
        --cert-file      /acme.sh/dblog.pl/cert.pem  \
        --key-file       /acme.sh/dblog.pl/key.pem  \
        --fullchain-file /acme.sh/dblog.pl/fullchain.pem

# Ask for sudo password
sudo -v

fullchain=$(sudo cat ./certs/dblog.org/fullchain.pem)
key=$(sudo cat ./certs/dblog.org/key.pem)

curl --request PUT \
    --header "PRIVATE-TOKEN: $gitlab_token" \
    --form "value=$key" \
    "https://gitlab.com/api/v4/groups/5176975/variables/ENGRAVE_SSL_KEY" | jq

curl --request PUT \
    --header "PRIVATE-TOKEN: $gitlab_token" \
    --form "value=$fullchain" \
    "https://gitlab.com/api/v4/groups/5176975/variables/ENGRAVE_SSL_FULLCHAIN" | jq

fullchain=$(sudo cat ./certs/dblog.pl/fullchain.pem)
key=$(sudo cat ./certs/dblog.pl/key.pem)

curl --request PUT \
    --header "PRIVATE-TOKEN: $gitlab_token" \
    --form "value=$key" \
    "https://gitlab.com/api/v4/groups/5176975/variables/DBLOG_SSL_KEY" | jq

curl --request PUT \
    --header "PRIVATE-TOKEN: $gitlab_token" \
    --form "value=$fullchain" \
    "https://gitlab.com/api/v4/groups/5176975/variables/DBLOG_SSL_FULLCHAIN" | jq

echo "NOTE: you need to trigger the pipeline to deploy the new certificates."