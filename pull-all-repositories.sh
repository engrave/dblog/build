#!/bin/bash
set -e

# Define the base GitLab group project path
BASE_PATH="git@gitlab.com:engrave/dblog"

# Function to update a repository (checkout master, pull, and update submodules)
update_repository() {
    local repo_path=$1

    cd "$repo_path"
    git checkout master
    git pull
    git submodule update --init --recursive
    cd ..
}

echo " * Preparing necessary directories"
mkdir -p ../backend
mkdir -p ../frontend

echo " * Updating all backend repositories"

backend=(
    "api-gateway"
    "blockchain-tracker"
    "domains-manager"
    "image-uploader"
    "mailer"
    "nginx-configurator"
    "sitemap-builder"
    "ssl-manager"
    "vault-connector"
    "webpush-sender"
)

for backend_repo in "${backend[@]}"; do
    update_repository "../backend/$backend_repo"
done

echo " * Updating all frontend repositories"

frontend=(
    "blogs-renderer"
    "main-website"
)

for frontend_repo in "${frontend[@]}"; do
    update_repository "../frontend/$frontend_repo"
done

echo " * Updating all top-level repositories"

toplevel=(
    "nginx"
    "shared-library"
    "gitlab-shared"
)

for toplevel_repo in "${toplevel[@]}"; do
    update_repository "../$toplevel_repo"
done

exit 0
