#!/bin/bash

read -p 'Domain for which you want to generate SSL certificate: ' domain

echo I will try to generate SSL certificates for $domain

sudo mkdir -p /var/engrave/nginx/certificates/owned/$domain

# Create a 2048 bit private key
sudo openssl genrsa -out "/var/engrave/nginx/certificates/owned/$domain/key.pem" 2048

# Create CSR for generated private key
sudo openssl req -new \
  -key "/var/engrave/nginx/certificates/owned/$domain/key.pem" \
  -out "/var/engrave/nginx/certificates/owned/$domain/$domain.csr" \
  -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=*.$domain"

# Generate certificate
sudo openssl x509 -req -days 365 \
  -in "/var/engrave/nginx/certificates/owned/$domain/$domain.csr" \
  -signkey "/var/engrave/nginx/certificates/owned/$domain/key.pem"  \
  -out "/var/engrave/nginx/certificates/owned/$domain/fullchain.pem"